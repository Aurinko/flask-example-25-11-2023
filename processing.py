import tensorflow as tf


def get_message(param):
    model = tf.keras.models.load_model("titanic_mlp")
    y_pred = model.predict(param)
    return f"Пассажир выжил с вероятностью {y_pred[0][0]}"

