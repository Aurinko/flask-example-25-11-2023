from flask import Flask, render_template, request

from processing import get_message

app = Flask(__name__)


# http://127.0.0.1:5000 + '/' = http://127.0.0.1:5000/
@app.route('/', methods=['get', 'post'])
def main():
    message = ''
    if request.method == "POST":
        pclass = float(request.form.get("pclass"))
        sex = float(request.form.get("sex"))
        age = float(request.form.get("age"))
        sibsp = float(request.form.get("sibsp"))
        parch = float(request.form.get("parch"))
        fare = float(request.form.get("fare"))

        person = [[pclass, sex, age, sibsp, parch, fare]]

        message = get_message(person)

    return render_template('index.html', message=message)


@app.route('/text')  # http://127.0.0.1:5000 + '/text' = http://127.0.0.1:5000/text
def text():
    return "Text"


app.run()
